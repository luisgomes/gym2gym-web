// código base de Luiz Otávio Miranda <contato@tutsup.com>

/**
 * create_cnpj_validator
 * 
 * creates a new validation object
 * 
 * @return object
 */
function create_cnpj_validator() {

    function verify(cnpj) {
        let original_cnpj = cnpj.toString()// keep the original input as a string

        let transformed_cnpj = original_cnpj.replace(/[^0-9]/g, '')// remove everything that is not a number

        let first_numbers_cnpj = transformed_cnpj.slice(0, 12)// get all the 12 first numbers

        /**
         * Multiply the CNPJ
         *
         * @param string    cnpj the CNPJ digits
         * @param int       position the position to begin the regression
         * 
         * @return int      the evaluated digit
         */
        function multiply_cnpj(cnpj, position = 5) {
            let evaluation = 0// keeps the sum of evaluations

            for (let i = 0; i < cnpj.length; i++) {// for loop through the cnpj string length

                // sum the var by itself then the current CNPJ digit x its position
                evaluation = evaluation + (parseInt(cnpj[i]) * position);

                // decrease the current position
                position--

                // if position lower than 2, becomes 9
                if (position < 2) position = 9
            }
            // returns the sum
            return evaluation
        }

        // do the first evaluation
        let first_evaluation = multiply_cnpj(first_numbers_cnpj);

        // if the rest of the division between the first calculation and 11 is lower than 2, the first
        // digit is 0, otherwise it is 11 minus the rest of the division of the calculation by 11
        let first_digit = (first_evaluation % 11) < 2 ? 0 : 11 - (first_evaluation % 11)

        // concat the first digit to the  first 12 of the CNPJ
        // now we have 13 numbers here
        first_numbers_cnpj += first_digit;

        // same as first evaluation, starting at position 6
        second_evaluation = multiply_cnpj(first_numbers_cnpj, 6)
        second_digit = (second_evaluation % 11) < 2 ? 0 : 11 - (second_evaluation % 11)

        // concat the second digit to the CNPJ
        evaluated_cnpj = first_numbers_cnpj + second_digit;

        // verify if the input CNPJ and the calculated CNPJ are the same
        if (evaluated_cnpj === transformed_cnpj) return true

        return false
    }

    return {
        verify
    }
}

function init() {
    const validator = create_cnpj_validator()// create validator

    var first_try = true;

    var first_two = "";
    var second_three = "";
    var third_three = "";
    var fourth_four = "";
    var last_two = "";

    do {
        if (!first_try)
            alert("Desculpe, CNPJ informato não é válido");

        alert("Por favor, insira o CNPJ " + ((first_try) ? "" : "correto ") + "da sua academia/empresa conforme os dígitos são pedidos.\nFormato -> ##.###.###/####-##");
        
        cnpj = "";

        var proceed_1 = false;
        do {
            proceed_1 = false;
            first_two = onlyNumbers(prompt("Insira os dois primeiros dígitos do CNPJ da sua academia\nFormato -> __.###.###/####-##"))
            if (first_two.length == 2) {
                proceed_1 = true
                cnpj += first_two;
            }
        } while (!proceed_1)

        var proceed_2 = false;
        do {
            proceed_2 = false;
            second_three = onlyNumbers(prompt("Insira os três dígitos seguintes do CNPJ da sua academia\nFormato -> " + first_two + ".___.###/####-##"))
            if (second_three.length == 3) {
                proceed_2 = true
                cnpj += second_three;
            }
        } while (!proceed_2)

        var proceed_3 = false;
        do {
            proceed_3 = false;
            third_three = onlyNumbers(prompt("Insira os três dígitos seguintes do CNPJ da sua academia\nFormato -> " + first_two + "." + second_three + ".___/####-##"))
            if (third_three.length == 3) {
                proceed_3 = true
                cnpj += third_three;
            }
        } while (!proceed_3)

        var proceed_4 = false;
        do {
            proceed_4 = false;
            fourth_four = onlyNumbers(prompt("Insira os quatro dígitos seguintes do CNPJ da sua academia\nFormato -> " + first_two + "." + second_three + "." + third_three + "/____-##"))
            if (fourth_four.length == 4) {
                proceed_4 = true
                cnpj += fourth_four;
            }
        } while (!proceed_4)

        var proceed_5 = false;
        do {
            proceed_5 = false;
            last_two = onlyNumbers(prompt("Insira os últimos dois dígitos (dígitos verificadores) do CNPJ da sua academia\nFormato -> " + first_two + "." + second_three + "." + third_three + "/" + fourth_four + "-__"))
            if (last_two.length == 2) {
                proceed_5 = true
                cnpj += last_two;
            }
        } while (!proceed_5)

        first_try = false;
    } while (cnpj == null || cnpj == "" || cnpj.length < 14 || validator.verify(cnpj) == false)
}