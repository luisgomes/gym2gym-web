let test = false;
function getMe(elem) {
    elem = document.getElementById(elem);
    if (elem != null) {
        return elem;
    } else {
        return false;
    }
}

function onlyNumbers(str) {
    if (typeof str != "string")
        return ""
    return str.toString().trim().replace(/[^0-9]/gm, '')
}

function resetForm() {
    $("#btn-accept").attr("disabled", true);
    $("#accept-checkbox").prop("checked", false);
}

function isAccepted() {
    return $("#accept-checkbox").is(":checked");
}

function enableAccept() {
    $("#btn-accept").attr("disabled", !isAccepted());
}

function acccept() {
    if (isAccepted()) {
        $('#modal-regulamento').modal('hide');
        $('#modal-firebase-simulation').modal('show');
    } else {
        console.log(false);
    }
}

function isFormValid() {
    const name = $("#nameInput").val();
    const lastName = $("#sobrenomeInput").val();
    const email = $("#emailInput").val();
    const accessKey = $("#keyInput").val();

    return accessKey.length > 0;
}

function getQueryParams(user) {
    return `/def/field10=${user.Nome}&field11=${user.Sobrenome}&field13=${user.Perfil}&field17=${user.Plan}&field19=${user.EmailTrabalho}&field20=${user.Email}&field23=${user.CNPJ}&field25=${user.RazaoSocial}&field27=${user.Endereco1}&field28=${user.Endereco2}&field29=${user.Cidade}&field30=${user.Estado}&field31=${user.CEP}&field32=${user.Pais}&field35=${user.Telefone}&field37=${user.Key}&field38=${user.Cargo}&field140=${user.Verificado}`;
}
// 
function checkFirebase() {
    if (isFormValid()) {
        const accessKey = $('#keyInput').val();
        const database = firebase.database().ref();
        const cadastroRef = database.child('users').child('cadastro');
        const query = cadastroRef.orderByChild('Key').equalTo(accessKey).limitToFirst(1);

        query.once('value', (snapshot) => {
            let user = null;
            snapshot.forEach(function (childSnapshot) {
                user = childSnapshot.val();
            });
            if (user) {
                console.table(user);
                console.log("https://makersapp.wufoo.com/forms/zua0oio1ci469v" + getQueryParams(user));
                if (!test) {
                    window.open("https://makersapp.wufoo.com/forms/zua0oio1ci469v" + getQueryParams(user), "_self");
                }
            } else {
                if (!test) {
                    window.open("https://makersapp.wufoo.com/forms/zua0oio1ci469v" + `/def/&field19=${$("#emailInput").val()}&field37=${$("#keyInput").val()}&field10=${$("#nameInput").val()}&field11=${$("#sobrenomeInput").val()}`, "_self");
                }
                console.warn('Usuário não encontrado!');
            }
        }, (errorObject) => {
            console.log('Erro ao buscar usuário: ' + errorObject.code);
        });
    }
}