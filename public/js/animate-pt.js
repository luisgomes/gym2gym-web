const slider_animation = {
    fps: 30,
    in_duration: 30 * 1,// time in frames from the beginning to the end of the alpha animation
    rest_duration: 30 * 6,// time in frames from the end of the alpha animation to the beginning of the next one
    current_frame: 0,
    front_element: "iPhoneXImageFront",
    back_element: "iPhoneXImageBack",
    image_changed: false,
    current_front_img: 0,
    current_back_img: 1,
    images_number: 4,
    animation: null
};

const slider_animation2 = {
    fps: 30,
    in_duration: 30 * 1,// time in frames from the beginning to the end of the alpha animation
    rest_duration: 30 * 6,// time in frames from the end of the alpha animation to the beginning of the next one
    current_frame: 0,
    front_element: "iPhoneXImageFront2",
    back_element: "iPhoneXImageBack2",
    image_changed: false,
    current_front_img: 0,
    current_back_img: 1,
    images_number: 2,
    animation: null
};

/**
 * animate
 * 
 * Animates a css property of the defined element from a defined start number to another number during the time
 * 
 * @param {HTMLObject} element 
 * @param {} attribute
 * @param {Number} from 
 * @param {Number} to 
 * 
 * @example
 * animate(document.getElementById("box") ,200 ,400 );
 */
function animate() {
    let front = getMe(slider_animation.front_element);
    let amount_to_update = 100 / slider_animation.in_duration;
    if (slider_animation.current_frame <= slider_animation.in_duration) {
        set_opacity(front, (slider_animation.in_duration - slider_animation.current_frame) * amount_to_update);
    } else if (slider_animation.current_frame <= slider_animation.rest_duration) {
        set_opacity(front, 0);
    } else {
        if (!slider_animation.image_changed) {
            slider_animation.image_changed = true;
            slider_animation.current_front_img++;
            if (slider_animation.current_front_img >= slider_animation.images_number) {
                slider_animation.current_front_img = 0;
            }
            slider_animation.current_back_img++;
            if (slider_animation.current_back_img >= slider_animation.images_number) {
                slider_animation.current_back_img = 0;
            }
            getMe(slider_animation.front_element).style.background = "URL(../images/iPhoneX0" + slider_animation.current_front_img + ".png)";
            getMe(slider_animation.back_element).style.background = "URL(../images/iPhoneX0" + slider_animation.current_back_img + ".png)";
        }
        slider_animation.image_changed = false;
        slider_animation.current_frame = 0;
        set_opacity(front, 100);
    }

    slider_animation.current_frame++;
}

function set_opacity(element, amount) {
    element.style.opacity = amount / 100;

    /* IE */
    element.style.filter = "alpha(opacity=" + parseInt(amount) + ")";
}

setTimeout(function () { slider_animation.animation = setInterval(animate, 1000 / slider_animation.fps) }, slider_animation.rest_duration * slider_animation.fps);

function animatePt() {
    let front = getMe(slider_animation2.front_element);
    let amount_to_update = 100 / slider_animation2.in_duration;
    if (slider_animation2.current_frame <= slider_animation2.in_duration) {
        set_opacity2(front, (slider_animation2.in_duration - slider_animation2.current_frame) * amount_to_update);
    } else if (slider_animation2.current_frame <= slider_animation2.rest_duration) {
        set_opacity2(front, 0);
    } else {
        if (!slider_animation2.image_changed) {
            slider_animation2.image_changed = true;
            slider_animation2.current_front_img++;
            if (slider_animation2.current_front_img >= slider_animation2.images_number) {
                slider_animation2.current_front_img = 0;
            }
            slider_animation2.current_back_img++;
            if (slider_animation2.current_back_img >= slider_animation2.images_number) {
                slider_animation2.current_back_img = 0;
            }
            getMe(slider_animation2.front_element).style.background = "URL(../images/iphoneX2" + slider_animation2.current_front_img + ".png)";
            getMe(slider_animation2.back_element).style.background = "URL(../images/iphoneX2" + slider_animation2.current_back_img + ".png)";
        }
        slider_animation2.image_changed = false;
        slider_animation2.current_frame = 0;
        set_opacity2(front, 100);
    }

    slider_animation2.current_frame++;
}

function set_opacity2(element, amount) {
    element.style.opacity = amount / 100;

    /* IE */
    element.style.filter = "alpha(opacity=" + parseInt(amount) + ")";
}

setTimeout(function () { slider_animation.animation = setInterval(animate, 1000 / slider_animation.fps) }, slider_animation.rest_duration * slider_animation.fps);
setTimeout(function () { slider_animation2.animation = setInterval(animatePt, 1000 / slider_animation2.fps) }, slider_animation2.rest_duration * slider_animation2.fps);

